package core;

import command.Command;
import command.CommandType;
import music.Music;
import music.MusicList;
import music.MusicNotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MusicStore implements IMusicStore {
    private MusicList musics;
    private ArrayList<Command> commands;
    private String dataFilePath, outputFilePath;
    private int cash;

    MusicStore() {
        musics = new MusicList();
        commands = new ArrayList<>();
        cash = 0;
    }

    @Override
    public void setOutputFilePath(String path) {
        this.outputFilePath = path;
    }

    @Override
    public void loadData(String path) {
        try {
            this.dataFilePath = path;
            for (String line : Files.readAllLines(Paths.get(path))) {
                String[] entries = line.split("[;]");
                if (entries.length == 6) {
                    musics.add(new Music(Integer.parseInt(entries[0]),
                            Integer.parseInt(entries[1]),
                            entries[2], entries[3].trim(),
                            Integer.parseInt(entries[4]),
                            Integer.parseInt(entries[5])));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadInput(String path) {
        try {
            for (String line : Files.readAllLines(Paths.get(path))) {
                line = line.replace("\"", "");
                String regex = line.split("\\s").length == 2 && !line.contains(":") ? "\\s" : "([\\s][a-zA-Z]+:)";
                List<String> entries = new LinkedList<>(Arrays.asList(line.split(regex)));
                try {
                    CommandType type = CommandType.valueOf(entries.get(0).toUpperCase());
                    entries.remove(0);
                    commands.add(new Command(type,
                            entries.toArray(new String[0]), Pattern.compile(regex).matcher(line)));
                } catch (IllegalArgumentException e) {
                    System.err.printf("ERR: %s? What do you mean?\n", entries.get(0));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processCommands() {
        StringBuilder output = new StringBuilder();
        for (Command command : commands) {
            String[] args = command.getArgs();
            Matcher matcher = command.getMatcher();
            switch (command.getType()) {
                case ADD:
                    Music m = new Music();
                    for (int i = 0; i < args.length && matcher.find(); i++) {
                        String section = matcher.group();
                        if (section.contains("id")) {
                            m.setId(Integer.valueOf(args[i]));
                        } else if (section.contains("name")) {
                            m.setName(args[i]);
                        } else if (section.contains("singer")) {
                            m.setSinger(args[i]);
                        } else if (section.contains("year")) {
                            m.setYear(Integer.valueOf(args[i]));
                        } else if (section.contains("count")) {
                            m.setCount(Integer.valueOf(args[i]));
                        } else if (section.contains("price")) {
                            m.setPrice(Integer.valueOf(args[i]));
                        }
                    }
                    musics.add(m);
                    output.append(String.format(Locale.US,
                            "New CD added id: %d name:\"%s\"\n", m.getId(), m.getName()));
                    break;
                case REMOVE:
                    try {
                        musics.removeId(Integer.valueOf(args[0]));
                        output.append(String.format(Locale.US, "CD removed id: %s\n", args[0]));
                    } catch (MusicNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
                case LIST:
                    output.append(String.format(Locale.US, "List\n%s\n", musics.getHelper().getTable()));
                    break;
                case EDIT:
                    if (matcher.find()) {
                        try {
                            Music music = musics.getById(Integer.valueOf(args[0]));
                            for (int i = 1; i < args.length && matcher.find(); i++) {
                                String section = matcher.group();
                                if (section.contains("name")) {
                                    music.setName(args[i]);
                                } else if (section.contains("singer")) {
                                    music.setSinger(args[i]);
                                } else if (section.contains("year")) {
                                    music.setYear(Integer.valueOf(args[i]));
                                } else if (section.contains("count")) {
                                    music.setCount(Integer.valueOf(args[i]));
                                } else if (section.contains("price")) {
                                    music.setPrice(Integer.valueOf(args[i]));
                                }
                            }
                            List<Music> list = new ArrayList<>();
                            list.add(music);
                            output.append(String.format(Locale.US, "Edit CD id: %d\n%s\n",
                                    music.getId(),
                                    musics.getHelper().generateTable(list)));
                        } catch (MusicNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case SEARCH:
                    output.append(String.format(Locale.US, "Search \"%s\"\n%s\n",
                            args[0], musics.getHelper().search(args[0])));
                    break;
                case SELL:
                    try {
                        Music music = musics.getById(Integer.valueOf(args[0]));
                        if (music.getCount() != 0) {
                            music.setCount(music.getCount() - 1);
                            music.setSold(music.getSold() + 1);
                            cash += music.getPrice();
                            output.append(String.format(Locale.US, "CD sold id: %d\n", music.getId()));
                        } else {
                            System.err.println("ERR: Not available.");
                        }
                    } catch (MusicNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
                case QUIT:
                    Music bestSeller = musics.getHelper().getBestSeller();
                    output.append(String.format(Locale.US, "Quit\nCash: %d\nBest Seller: %s\n", cash,
                            bestSeller.getName()));
                    break;
            }
        }
        try {
            Files.write(Paths.get(outputFilePath),
                    output.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
            Files.write(Paths.get(dataFilePath),
                    musics.getHelper().getData().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
