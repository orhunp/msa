package core;

import java.io.File;

public class Main {

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("ERR: Did you provide enough arguments?");
            return;
        }
        for (String arg : args) {
            if (!new File(arg).exists()) {
                System.err.printf("ERR: File not found: '%s'\n", arg);
                return;
            }
        }
        MusicStore musicStore = new MusicStore();
        musicStore.loadInput(args[0]);
        musicStore.loadData(args[1]);
        musicStore.setOutputFilePath(args[2]);
        musicStore.processCommands();
    }
}
