package core;

public interface IMusicStore {
    void setOutputFilePath(String path);
    void loadData(String path);
    void loadInput(String path);
    void processCommands();
}
