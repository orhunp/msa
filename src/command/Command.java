package command;

import java.util.regex.Matcher;

public class Command {
    private CommandType type;
    private String[] args;
    private Matcher matcher;

    public Command(CommandType type, String[] args, Matcher matcher) {
        this.type = type;
        this.args = args;
        this.matcher = matcher;
    }

    public CommandType getType() {
        return type;
    }

    public String[] getArgs() {
        return args;
    }

    public Matcher getMatcher() {
        return matcher;
    }
}
