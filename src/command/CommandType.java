package command;

public enum CommandType {
    ADD,
    REMOVE,
    LIST,
    EDIT,
    SEARCH,
    SELL,
    QUIT,
}
