package music;

import java.util.List;

public interface IMusicListHelper {
    List<Music> asList();
    String generateTable(List<Music> list);
    String getTable();
    String search(String query);
    String getData();
    Music getBestSeller();
}
