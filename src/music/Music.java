package music;

import java.util.Locale;

public class Music {
    private int id, price, year, count, sold;
    private String name, singer;

    public Music() {
    }

    public Music(int id, int price, String name, String singer, int year, int count) {
        this.id = id;
        this.price = price;
        this.name = name;
        this.singer = singer;
        this.year = year;
        this.count = count;
        this.sold = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    private int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    String toCompressedString() {
        return String.format(Locale.US, "%d;%d;%s;%s;%d;%d",
                this.getId(), this.getPrice(),
                this.getName(), this.getSinger(),
                this.getYear(), this.getCount());
    }

    @Override
    public String toString() {
        return String.format(Locale.US,
                "%-2d %-5d %-25s %-20s %-2d %-2d",
                this.id, this.price, this.name,
                this.singer, this.year, this.count);
    }
}
