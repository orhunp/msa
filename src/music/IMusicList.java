package music;;

public interface IMusicList {
    int size();
    void add(Music music);
    void remove(Music music);
    void removeId(int id) throws MusicNotFoundException;
    Music get(int i) throws MusicNotFoundException;
    Music getById(int id) throws MusicNotFoundException;
    boolean contains(Music music);
}
