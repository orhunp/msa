package music;

public class MusicList implements IMusicList {
    private MusicNode head;

    static class MusicNode {
        Music music;
        MusicNode next;

        MusicNode(Music music) {
            this.music = music;
            this.next = null;
        }
    }

    public MusicList() {
        this.head = null;
    }

    @Override
    public int size() {
        int size = 0;
        MusicNode current = head;
        while (current != null) {
            size++;
            current = current.next;
        }
        return size;
    }

    @Override
    public void add(Music music) {
        if (this.contains(music)) {
            System.out.printf("WARN: List already contains \"%s\" (%d), cannot add.\n",
                    music.getName(), music.getId());
            return;
        }
        MusicNode musicNode = new MusicNode(music);
        if (head == null) {
            head = musicNode;
        } else {
            MusicNode last = head;
            while (last.next != null) {
                last = last.next;
            }
            last.next = musicNode;
        }
    }

    @Override
    public void remove(Music music) {
        MusicNode current = head;
        MusicNode previous = current;
        while (current != null) {
            if (music.getId() == current.music.getId()) {
                if (current == head) {
                    head = head.next;
                } else {
                    previous.next = current.next;
                }
                break;
            }
            previous = current;
            current = current.next;
        }
    }

    @Override
    public void removeId(int id) throws MusicNotFoundException {
        this.remove(this.getById(id));
    }

    @Override
    public Music get(int i) throws MusicNotFoundException {
        int index = 0;
        MusicNode current = head;
        while (current != null) {
            if (i == index) {
                return current.music;
            }
            index++;
            current = current.next;
        }
        throw new MusicNotFoundException("Cannot find music by index: " + i);
    }

    @Override
    public Music getById(int id) throws MusicNotFoundException {
        for (int i = 0; i < this.size(); i++) {
            if (this.get(i).getId() == id) {
                return this.get(i);
            }
        }
        throw new MusicNotFoundException("Cannot find music by id: " + id);
    }

    @Override
    public boolean contains(Music music) {
        MusicNode current = head;
        while (current != null) {
            if (music.getId() == current.music.getId()) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    public MusicListHelper getHelper() {
        return new MusicListHelper(head);
    }
}
