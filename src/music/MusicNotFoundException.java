package music;

public class MusicNotFoundException extends Exception {
    MusicNotFoundException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
