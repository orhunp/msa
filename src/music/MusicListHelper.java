package music;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class MusicListHelper implements IMusicListHelper {

    private MusicList.MusicNode head;

    MusicListHelper(MusicList.MusicNode head) {
        this.head = head;
    }

    @Override
    public List<Music> asList() {
        List<Music> list = new ArrayList<>();
        MusicList.MusicNode current = head;
        while (current != null) {
            list.add(current.music);
            current = current.next;
        }
        return list;
    }

    @Override
    public String generateTable(List<Music> list) {
        list.sort((Music m1, Music m2) -> m1.getName().compareToIgnoreCase(m2.getName()));
        StringBuilder table = new StringBuilder();
        table.append("Id Price Name\t\t\t\t\t   Singer\t\t\t\tYear Count\n");
        table.append("-- ----- ----\t\t\t\t\t   ------\t\t\t\t---- -----\n");
        for (Music music : list) {
            table.append(music.toString());
            table.append("\n");
        }
        return table.toString();
    }

    @Override
    public String getTable() {
        return this.generateTable(this.asList());
    }

    @Override
    public String search(String query) {
        List<Music> list = this.asList();
        list.removeIf(m -> !m.getName().toLowerCase().contains(query.toLowerCase()));
        return this.generateTable(list);
    }

    @Override
    public String getData() {
        StringBuilder data = new StringBuilder();
        MusicList.MusicNode current = head;
        while (current != null) {
            data.append(current.music.toCompressedString());
            data.append("\n");
            current = current.next;
        }
        return data.toString();
    }

    @Override
    public Music getBestSeller() {
        List<Music> list = this.asList();
        list.sort(Comparator.comparingInt(Music::getSold));
        return list.get(list.size() - 1);
    }
}
